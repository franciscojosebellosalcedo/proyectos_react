import React from "react";

export function Modal(props) {
    

    if(props.tipo==="login"){
        return(
            <section className="modal" id="login">
                    <div className="modal__container">
                        <h4 className="modal__cerrar" onClick={()=>props.fnAbrirModalLogin(false)}>X</h4>
                        <h2 className="modal__titulo">Login</h2>
                        <form className="modal__formulario">
                            <label for="correo" className="modal__label">Correo</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="email" id="correo" name="correo" placeholder="admin"/>
                        
                            <label for="contraseña" className="modal__label">Contraseña</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="password" id="contraseña" name="Contrasenia" placeholder="admin"/>
                            <button className="modal__iniciarSesion" onClick={props.fnValidarUsuario} >Iniciar Sesion</button>
                        </form>
                    </div>
            </section>
        );
    }else if(props.tipo==="mensaje"){
        return(
            <section className="modal" id="mensaje">
                    <div className="modal__container container__mensaje">
                        <h2 className="modal__titulo">Mensaje</h2>
                        <h3 className="modal__mensaje">{props.mensaje}</h3>
                    </div>
            </section>
        );
    }else if(props.tipo==="agregar"){
        return(
            <section className="modal" id="agregar">
                    <div className="modal__container container__agregar">
                        <h4 className="modal__cerrar" onClick={()=>props.fnAbrirModalAgregar(false)}>X</h4>
                        <h2 className="modal__titulo">Agregar Persona Nueva</h2>
                        <form className="modal__formulario">
                            <label for="nombre" className="modal__label">Nombre</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre"/>

                            <label for="apellido" className="modal__label">Apellido</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="text" id="apellido" name="apellido" placeholder="Ingrese el apellido"/>
                            
                            <label for="documento" className="modal__label">Documento</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="number" id="documento" name="documento" placeholder="Ingrese el documento"/>

                            <label for="edad" className="modal__label">Edad</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="number" id="edad" name="edad" placeholder="Ingrese el edad"/>
                            
                            <div className="modal__botones">
                                <a className="modal__btn__guardar" onClick={()=>props.fnAgregarPersona()}>Guardar</a>
                                <a className="modal__btn__salir" onClick={()=>props.fnAbrirModalAgregar(false)} >Salir</a>
                            </div>
                        </form>
                    </div>
            </section>
        );
    }else if(props.tipo==="editar"){
        return(
            <section className="modal" id="editar">
                    <div className="modal__container container__editar">
                        <h4 className="modal__cerrar" onClick={()=>props.fnAbrirModalEditar(false)}>X</h4>
                        <h2 className="modal__titulo">Editar Persona</h2>
                        <form className="modal__formulario">
                            <label for="nombre" className="modal__label">Nombre</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="text" id="nombre__editar" name="nombre" placeholder="Ingrese el nombre"/>

                            <label for="apellido" className="modal__label">Apellido</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="text" id="apellido__editar" name="apellido" placeholder="Ingrese el apellido"/>
                            
                            <label for="documento" className="modal__label">Documento</label>
                            <input onChange={props.fnGuardarDatos}  value={props.documento}  className="modal__input" type="number" id="documento__editar" name="documento" placeholder="Ingrese el documento"/>

                            <label for="edad" className="modal__label">Edad</label>
                            <input onChange={props.fnGuardarDatos} className="modal__input" type="number" id="edad__editar" name="edad" placeholder="Ingrese el edad"/>
                            
                            <div className="modal__botones">
                                <a className="modal__btn__guardar" onClick={()=>props.fnEditarPersona()}>Guardar</a>
                                <a className="modal__btn__salir" onClick={()=>props.fnAbrirModalEditar(false)} >Salir</a>
                            </div>
                        </form>
                    </div>
            </section>
        );
    }
}