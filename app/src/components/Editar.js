import React,{useState} from 'react';
import { Modal } from './Modal';

function Editar(props) {
    const [nuevosDatos, setNuevosDatos] = useState(
        {nombre:"",apellido:"",edad:""}
    );
    const [mensaje,setMensaje]=useState("holaaaaaa");

    function abrirModalEditar(mostrar) {
        let editar=document.getElementById("editar");
        editar.style="z-index:200;";
        let container__editar=document.querySelector(".container__editar");
        container__editar.style="height:600px;"
         if(mostrar){
             editar.classList.add("mostrarModal");
         }else{
             editar.classList.remove("mostrarModal");
         }
 
    }
    function validarCampos() {
        let nombre=document.getElementById("nombre__editar").value;
        let apellido=document.getElementById("apellido__editar").value;
        let edad=document.getElementById("edad__editar").value;

        if (nombre==="" || apellido==="" || edad==="") {
            return true;
        }else{
            return false;
        }
        
    }
    function limpiarCampos() {
        document.getElementById("nombre__editar").value="";
        document.getElementById("apellido__editar").value="";
        document.getElementById("edad__editar").value="";
    }
    function editarPersona() {
        if(validarCampos()===true){
            setMensaje("Llene los campos por favor");
            abrirModalMensaje(true);
            setTimeout(() => {
                abrirModalMensaje(false);
            }, 2000);
        }else{
            let lista=props.lista;
            let persona=props.persona;
            for (const index in lista) {
                if(persona.documento===lista[index].documento){
                    lista[index].nombre=nuevosDatos.nombre;
                    lista[index].apellido=nuevosDatos.apellido;
                    lista[index].edad=nuevosDatos.edad;
                }
                console.log(index)
            }
            limpiarCampos();
            props.fnSetListaPersona([...lista]);
            console.log(lista)
        }
    }
    function guardarDatos(e) {
        setNuevosDatos(
            {...nuevosDatos,[e.target.name]:e.target.value}
        );
    }
    function abrirModalMensaje(mostrar) {
        let modal=document.getElementById("mensaje");
        modal.style="z-index:300";
        let container__mensaje=document.querySelector(".container__mensaje");
        container__mensaje.style="height: 250px";
        let mensaje=document.getElementById("mensaje");
         if(mostrar){
             mensaje.classList.add("mostrarModal");
         }else{
             mensaje.classList.remove("mostrarModal");
         }
    }
    return(
        <div>
            <button className='btn__editar btn btn' onClick={()=>abrirModalEditar(true)}>Editar</button>
            <Modal
            tipo="editar"
            fnAbrirModalEditar={abrirModalEditar}
            fnGuardarDatos={guardarDatos}
            fnEditarPersona={editarPersona}
            documento={props.persona.documento}
            />

            <Modal
            tipo="mensaje"
            mensaje={mensaje}
            />
        </div>
    );
}


export default Editar;
