import React,{useEffect, useState} from "react";
import {Modal} from "./Modal";

function Login(props) {
    const [mensaje,setMensaje]=useState("");

    useEffect(()=>{
        console.log(formularioLogin);
    },[]);
    const [formularioLogin,setFormularioLogin]=useState(
        {correo:"",contrasenia:""},
    )

    function abrirModalLogin(mostrar) {
        let login=document.getElementById("login");
         if(mostrar){
             login.classList.add("mostrarModal");
         }else{
             login.classList.remove("mostrarModal");
         }
 
    }
    function abrirModalMensaje(mostrar) {
        let modal=document.getElementById("mensaje");
        modal.style="z-index:300";
        let modal__container=document.querySelector(".modal__container");
        modal__container.style="height: 250px";
        let mensaje=document.getElementById("mensaje");
         if(mostrar){
             mensaje.classList.add("mostrarModal");
         }else{
             mensaje.classList.remove("mostrarModal");
         }
    }

    function validarUsuario(e){
        e.preventDefault();
        let correo=document.getElementById("correo").value;
        let contrasenia=document.getElementById("contraseña").value;
        if(correo==="" || contrasenia===""){
            setMensaje("Llene los campos por favor");
            abrirModalMensaje(true);
            setTimeout(() => {
                abrirModalMensaje(false)
            }, 2000);
        }else{
            let usuarios=[props.usuario];
            usuarios.map((usu)=>{
                if((usu.correo===correo || usu.username==correo) && usu.contrasenia===contrasenia){
                    document.getElementById("correo").value="";
                    document.getElementById("contraseña").value="";
                    abrirModalLogin(false);
                    props.fnSetVentana(true);
                }else{
                    setMensaje("Usuario incorrecto");
                    abrirModalMensaje(true);
                    setTimeout(() => {
                        abrirModalMensaje(false)
                    }, 2000); 
                }
            })
        }
        
    }
   
    function guardarDatos(e) {
        setFormularioLogin({...formularioLogin,[e.target.name]:e.target.value});
        console.log(formularioLogin)
    }   

    return(

       <div>
         <div className="login">
            <h1 className="login__titulo">App</h1>
            <button className="login__iniciar" onClick={()=>abrirModalLogin(true)} >Iniciar</button>
        </div>
            <Modal
            tipo="mensaje"
            mensaje={mensaje}
            />
            <Modal
            tipo="login"
            fnAbrirModalLogin={abrirModalLogin}
            fnValidarUsuario={validarUsuario}
            fnGuardarDatos={guardarDatos}
            />
       </div>
    );
    

}

export default Login;


