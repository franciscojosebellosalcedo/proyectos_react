import React,{useState} from "react";
import { Modal } from "./Modal";

function Agregar(props) {
    const [nuevaPersona,setNuevaPersona]=useState(
        {
        nombre:"",apellido:"",documento:"",edad:""
        }
    );
    const [mensaje,setMensaje]=useState("");

    function guardarDatos(e) {
        setNuevaPersona(
            {...nuevaPersona,[e.target.name]:e.target.value}
        )
    }

    function abrirModalMensaje(mostrar) {
        let modal=document.getElementById("mensaje");
        modal.style="z-index:300";
        let container__mensaje=document.querySelector(".container__mensaje");
        container__mensaje.style="height: 250px";
        let mensaje=document.getElementById("mensaje");
         if(mostrar){
             mensaje.classList.add("mostrarModal");
         }else{
             mensaje.classList.remove("mostrarModal");
         }
    }
    function validarCampos() {
        let nombre=document.getElementById("nombre").value;
        let apellido=document.getElementById("apellido").value;
        let documento=document.getElementById("documento").value;
        let edad=document.getElementById("edad");
        if (nombre==="" || apellido==="" || documento==="" || edad==="") {
            return true;
        }else{
            return false;
        }
        
    }
    function limpiarCampos() {
        document.getElementById("nombre").value=null;
        document.getElementById("apellido").value=null;
        document.getElementById("documento").value=null;
        document.getElementById("edad").value=null;
    }

    function agregarPersona() {
        if(validarCampos()===true){
            setMensaje("Llene los campos por favor");
            abrirModalMensaje(true);
            setTimeout(() => {
                abrirModalMensaje(false);
            }, 2000);
        }else{
            
        } 
    }

    function abrirModalAgregar(mostrar) {
        let agregar=document.getElementById("agregar");
        agregar.style="z-index:200;";
        let container__agregar=document.querySelector(".container__agregar");
        container__agregar.style="height:600px;"
         if(mostrar){
             agregar.classList.add("mostrarModal");
         }else{
             agregar.classList.remove("mostrarModal");
         }
 
    }
    return (
        <div>
            <button onClick={()=>abrirModalAgregar(true)} className="btn__agregar btn">Agregar</button>
            <Modal
                tipo="agregar"
                fnGuardarDatos={guardarDatos}
                fnAbrirModalAgregar={abrirModalAgregar}
                fnAgregarPersona={agregarPersona}
            />
            <Modal
                tipo="mensaje"
                mensaje={mensaje}
            />
        </div>
    );
}

export default Agregar;