import React, { useState, createContext } from 'react';
import Login from './Login.js';
import Agregar from './Agregar.js';
import Editar from './Editar.js';
export const Contexto=createContext();

function App() {
  const[listaPersonas,setListaPersonas]=useState(
    [
        {nombre:"francisco",apellido:"bello salcedo",documento:"1007130073",edad:"20"},
        {nombre:"jose",apellido:"hernn",documento:"1307130073",edad:"30"},
        {nombre:"maria",apellido:"hernn",documento:"1307150073",edad:"30"},
      ]
    );
  const[usuario,setUsuario]=useState(
      {
          username:"admin",correo:"admin@gmail.com",contrasenia:"admin",
        }
      );
      const [ventana,setVentana]=useState(true);

      if(ventana===false){
          return (
              <Login
              usuario={usuario}
              fnSetVentana={setVentana}
              />
            );
          
          }else{
    return (
      <div className='app'>
        <h1 className='app__bienvenida'>Bienvenido {usuario.username}</h1>
        <div className="app__inicial">
          <h2 className="app__cantidadPersonas">Total de personas : <span>{listaPersonas.length}</span></h2>
          <div className="app__botones">
            <Agregar
            lista={listaPersonas}
            fnSetListaPersona={setListaPersonas}
            />
            <button>Cerrar sesion</button>
          </div>
        </div>
        <h2 className="titulo__tabla">Registros</h2>
        <div className="tabla">
        <table>
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Documento</th>
              <th>Edad</th>
              <th colSpan={2}>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {listaPersonas.map(persona=>(
              <tr key={persona.documento}>
                <td>{persona.nombre}</td>
                <td>{persona.apellido}</td>
                <td>{persona.documento}</td>
                <td>{persona.edad}</td>
                <td>
                  <Editar
                  lista={listaPersonas}
                  persona={persona}
                  fnSetListaPersona={setListaPersonas}
  
                  />
                </td>
                <td>eliminar</td>
              </tr>
            ))}
          </tbody>
        </table>
        </div>
      </div>
    );
  }

}


  
  
  export default App;