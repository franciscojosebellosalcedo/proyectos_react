import express from "express";
import cors from "cors";
import index_router from "./routers/index_rutas.js";
import tarea_router from "./routers/tareas_rutas.js";

const app =express();

app.use(cors());
app.use(express.json());
app.use(index_router);
app.use(tarea_router);

app.listen(4000);
console.log(`servidor en el  puerto 4000`);
