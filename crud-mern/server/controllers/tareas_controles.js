import { connection } from "../db.js"

export const obtenerTareas = async (req, res) => {
    try {
        const [resultado] = await connection.query("select id,titulo,descripcion,estado,fecha from tareas");
        if (resultado.length === 0) {
            return res.status(200).json({ mensaje: "no hay tareas" });
        } else {
            res.json(resultado);
        }
        console.log(resultado);
    } catch (error) {
        return res.status(500).json({ mensaje: "Error en el servidor" });
    }
}

export const obtenerTarea = async (req, res) => {
    try {
        console.log(req);
        console.log(req.params.id);
        const [resultado] = await connection.query("select * from tareas where id = ?", [req.params.id]);
        if (resultado.length === 0) {
            return res.status(404).json({ mensaje: "Tarea no encontrada" });
        } else {
            res.json(resultado[0]);
            console.log(resultado[0]);
        }
    } catch (error) {
        return res.status(500).json({ mensaje: "Error en el servidor" });
    }
}

export const agregarTarea = async (req, res) => {
    try {
        const { titulo, descripcion } = req.body;
        const [resultado] = await connection.query("insert into tareas (titulo,descripcion) values (?,?) ", [titulo, descripcion]);
        res.json({
            id: resultado.insertId,
            titulo: titulo,
            descripcion: descripcion
        });
        console.log(resultado);

    } catch (error) {
        return res.status(500).json({ mensaje: "Error en el servidor" });

    }
}

export const eliminarTarea = async (req, res) => {
    try {
        const [resultado] = await connection.query("delete from tareas where id = ?", [req.params.id]);
        if (resultado.affectedRows === 0) {
            return res.status(404).json({ mensaje: "Tarea no encontrada" });
        }
        return res.sendStatus(204);
    } catch (error) {
        return res.status(500).json({ mensaje: "Error en el servidor" });

    }
}

export const editarTarea = async (req, res) => {
    try {
        const [resultado] = await connection.query("update tareas set ?  where   id = ?", [req.body, req.params.id]);
        res.json(resultado)
        console.log(resultado);

    } catch (error) {
        return res.status(500).json({ mensaje: "Error en el servidor" });

    }
}