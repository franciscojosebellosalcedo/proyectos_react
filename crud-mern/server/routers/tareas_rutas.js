import { Router } from "express";
import {
    agregarTarea, editarTarea, eliminarTarea, obtenerTarea, obtenerTareas
} from "../controllers/tareas_controles.js";

const tarea_router = Router();

tarea_router.get("/tareas", obtenerTareas);

tarea_router.get("/tarea/:id", obtenerTarea);

tarea_router.post("/nueva_tarea", agregarTarea);

tarea_router.put("/editar_tarea/:id", editarTarea);

tarea_router.delete("/eliminar_tarea/:id", eliminarTarea);

export default tarea_router;

