import React from 'react';
import { NavLink,Link } from "react-router-dom";

export const NavBar = () => {
    return (
        <header className='header'>
            <nav className='menu'>
                <Link to="/"><figure className='menu__logo'>Logo</figure></Link>
                <ul className='menu__lista'>
                    <li className='menu__item item'>
                        <NavLink to="/nueva-tarea">Crear Tarea</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    )
}


export default NavBar;