import React from "react";
import Tareas from "./pages/Tareas.js";
import FormularioTarea from "./pages/FormularioTarea.js";
import PaginaNoEncontrada from "./pages/PaginaNoEncontrada.js";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import NavBar from "./components/NavBar.js";
import FormularioEditarTarea from "./pages/FormularioEditarTarea.js";


function App() {
  return (
    <div>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path="/" element={<Tareas />}></Route>
          <Route path="/nueva-tarea" element={<FormularioTarea />}></Route>
          <Route path="/editar-tarea/:id" element={<FormularioEditarTarea/>}></Route>
          <Route path="*" element={<PaginaNoEncontrada />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
