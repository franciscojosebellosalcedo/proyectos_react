import axios from "axios";

export const peticionAgregarTarea= async(tarea)=>{
    return await axios.post("http://localhost:4000/nueva_tarea",tarea);
}
export const peticionObtenerTareas= async()=>{
    return await axios.get("http://localhost:4000/tareas");
}

export const peticionEliminarTarea= async(id)=>{
    return await axios.delete(`http://localhost:4000/eliminar_tarea/${id}`);
}
