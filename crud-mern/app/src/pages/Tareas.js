import React, { useEffect, useState } from 'react'
import { peticionEliminarTarea, peticionObtenerTareas } from "../api/tarea.api.js";
import { Link } from "react-router-dom";
export const Tareas = () => {
  const [tareas, setTareas] = useState([]);

  async function obtenerTareas() {
    const respuesta  = await peticionObtenerTareas();
    setTareas([...respuesta.data])
  }
  useEffect(() => {
    obtenerTareas();
  }, []);

  async function eliminarTarea(id) {
    const resultado=await peticionEliminarTarea(id);
    obtenerTareas();
    console.log(resultado)
  }
  return (
    <div className='tareas'>
      <h1 className='tareas__titulo'>Tareas</h1>
      <div className='container__tareas'>
        {
          tareas.length>0 ?
          tareas.map((tarea) => (
            <div className='tarea' key={tarea.id}>
              <button className='tarea__btn__eliminar' onClick={()=>eliminarTarea(tarea.id)}>X</button>
              <div className='tarea__text'>
                <h2 className='tarea__titulo'>{tarea.titulo}</h2>
                <p className='tarea__descripcion'>{tarea.descripcion}</p>
                <Link to={`/editar-tarea/${tarea.id}`} className=" btn tarea__btn__editar">Editar</Link>
              </div>
            </div>
          ))
          :
          <h1>No hay tareas</h1>
        }
      </div>
    </div>
  )
}

export default Tareas;
