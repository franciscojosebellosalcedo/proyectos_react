import React, { useState } from 'react';
import { peticionAgregarTarea } from "../api/tarea.api.js";
export const FormularioTarea = () => {
  const [nuevaTarea, setNuevaTarea] = useState({
    titulo: "",
    descripcion: "",
  });
  function guardarDatos(e) {
    setNuevaTarea(
      { ...nuevaTarea, [e.target.name]: e.target.value }
    );
  }
  function validarCampos() {
    const { titulo, descripcion } = nuevaTarea;
    if (titulo === "" || descripcion === "") {
      return true;
    } else {
      return false;
    }
  }
  function limpiarCampos() {
    setNuevaTarea(
      { ...nuevaTarea, titulo: "", descripcion: "" }
    );
  }
  async function agregarTarea(e) {
    e.preventDefault();
    if (validarCampos() === true) {
      alert("Llene los campos por favor..");
    } else {
      try {
        const resultado = await peticionAgregarTarea(nuevaTarea);
        if (resultado.status === 200) {
          console.log("tarea agregada exitosamente");
        }
        limpiarCampos();
      } catch (error) {
        console.log(error);
      }
    }
  }
  return (
    <div className='container'>

      <h1 className='container__titulo'>Nueva Tarea</h1>
      <form className='formulario formulario__nueva__tarea'>
        <input value={nuevaTarea.titulo} onChange={guardarDatos} type="text" name='titulo' placeholder='Titulo de la tarea' className='input formulario__input__titulo' />
        <textarea value={nuevaTarea.descripcion} onChange={guardarDatos} name="descripcion" placeholder='Descripcion de la tarea' className='formulario__textarea__descripcion'></textarea>
        <button onClick={(e) => agregarTarea(e)} className='boton formulario__boton__agregar'>Agregar Tarea</button>
      </form>
    </div>
  )
}

export default FormularioTarea;
