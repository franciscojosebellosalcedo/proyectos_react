import React from 'react'

export const FormularioEditarTarea = () => {
    return (
        <div className='container'>

            <h1 className='container__titulo'>Editar Tarea</h1>
            <form className='formulario formulario__editar__tarea'>
                <input type="text"  className='input formulario__input__titulo' />
                <textarea  className='formulario__textarea__descripcion'></textarea>
                <button  className='boton formulario__boton__agregar'>Agregar Tarea</button>
            </form>
        </div>
    )
}

export default FormularioEditarTarea;
