import React, { useContext, useEffect, useState } from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { PersonasContext } from "../routers/Routers.js";

export const Editar = () => {
  const navegar = useNavigate();

  const [datosNuevos, setDatosNuevos] = useState({
    nombres: "",
    apellidos: "",
  });

  const { documento } = useParams();
  const { personas, setPersonas } = useContext(PersonasContext);

  
  useEffect(() => {
    const input_nombres = document.getElementById("nombres");
    const input_apellidos = document.getElementById("apellidos");
    const input_documento = document.getElementById("documento");

    personas.map((p) => {
      if (documento === p.documento) {
        input_nombres.placeholder = p.nombres;
        input_apellidos.placeholder = p.apellidos;
        input_documento.value = p.documento;
      }
    })
  });

  function guardarDatosNuevos(e) {
    setDatosNuevos(
      { ...datosNuevos, [e.target.name]: e.target.value }
    );
  }
  function validarCampos() {
    const { nombres, apellidos } = datosNuevos;
    if (nombres === "" || apellidos === "") {
      return "vacios";
    } else {
      return "llenos";
    }
  }

  const actualizarPersona = (e) => {
    e.preventDefault();

    if (validarCampos() === "vacios") {
      alert("Llene los campos para poder editar a la persona")
    } else {
      const arrayPersonas = personas;
      for (let i = 0; i < arrayPersonas.length; i++) {
        const p = arrayPersonas[i];
        if (documento === p.documento) {
          p.nombres = datosNuevos.nombres;
          p.apellidos = datosNuevos.apellidos;
        }
      }

      setPersonas([...arrayPersonas]);
      limpiarCampos();
      navegar("/");
    }
  }
  function limpiarCampos() {
    setDatosNuevos({...datosNuevos,nombres:"",apellidos:""});
  }

  return (
    <div>
      <form className='formulario__editar'>
        <label htmlFor="nombres">Nombres</label>
        <input value={datosNuevos.nombres} onChange={guardarDatosNuevos} type="text" name="nombres" id="nombres" className='input' />
        <label htmlFor="apellidos">Apellidos</label>
        <input value={datosNuevos.apellidos} onChange={guardarDatosNuevos} type="text" name="apellidos" id="apellidos" className='input' />
        <label htmlFor="documento">Documento</label>
        <input readOnly type="text" name='' id='documento' className='input' />
        <button className='btn' onClick={(e) => actualizarPersona(e)}>Guardar Cambios</button>
      </form>
    </div>
  )
}

export default Editar;
