import React from 'react'
import { Link } from 'react-router-dom';

export const NoEncontrada = () => {
  return (
    <div>NoEncontrada

      <Link to="/">Ir Al inicio</Link>
    </div>
  )
}
export default NoEncontrada;
