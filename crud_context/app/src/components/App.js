import React, { useContext, useState } from 'react';
import { Link } from "react-router-dom";

import { PersonasContext } from '../routers/Routers';
import Eliminar from './Eliminar';

export const App = () => {
  const { personas } = useContext(PersonasContext);


  if(personas.length>0){
    return (
      <div className='app'>
        <h1 className='app__titulo'>Registros</h1>
        <Link to="/agregar">Agregar Persona</Link>
        <table>
          <thead>
            <tr>
              <th>Nombres</th>
              <th>Apellidos</th>
              <th>Documento</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {
              personas.map((persona) => (
                <tr key={persona.documento}>
                  <td>{persona.nombres}</td>
                  <td>{persona.apellidos}</td>
                  <td>{persona.documento}</td>
                  <td><Eliminar documento={persona.documento}/></td>
                  <td><Link to={`/editar/${persona.documento}`}>Editar</Link></td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    )
  }else{
    return(
      <h1>No hay personas registradas</h1>
    );
  }
}

export default App;
