import React, { useContext } from 'react'
import { PersonasContext } from '../routers/Routers';
export const Eliminar = (props) => {
  const {personas,setPersonas}=useContext(PersonasContext);

  function eliminar() {
    const array=personas;
    array.map((p,i)=>{
      if(props.documento===p.documento){
        array.splice(i,1);
      }
    });
    setPersonas([...array]);
  }

  return (
    <button onClick={()=>eliminar()}>Eliminar</button>
  )
}

export default Eliminar;
