import React, { useState, useContext } from 'react';
import { useNavigate } from "react-router-dom";
import { PersonasContext } from '../routers/Routers';
export const Agregar = () => {
  const [personaNueva, setPersonaNueva] = useState({
    nombres: "",
    apellidos: "",
    documento: ""
  });
  const nav = useNavigate();
  const { personas, setPersonas } = useContext(PersonasContext);

  const guardarDatos = (e) => {
    setPersonaNueva({ ...personaNueva, [e.target.name]: e.target.value });
    console.log(personaNueva);
  }

  const agregarPersona = (e) => {
    e.preventDefault();
    if (validarCampos() === true) {
      alert("Llene los campos por favor");
    } else {
      if (validarPersona() === true) {
        alert("La persona ya existe");
      } else {
        setPersonas([...personas, personaNueva]);
        limpiarCampos();
        nav("/");
      }
    }
  }

  function validarPersona() {
    let contador=0;
    personas.map((p) => {
      if (personaNueva.documento === p.documento) {
        contador++;
      } 
    });
    if(contador>0) return true;
    else return false;
  }

  function validarCampos() {
    const { nombres, apellidos, documento } = personaNueva;
    if (nombres === "" || apellidos === "" || documento === "") {
      return true;
    } else {
      return false;
    }
  }
  function limpiarCampos() {
    setPersonaNueva({ ...personaNueva, nombres: "", apellidos: "", documento: "" });
  }
  return (
    <div className='container agregar'>
      <form className='formulario__agregar'>
        <label htmlFor="nombres">Nombres</label>
        <input value={personaNueva.nombres} onChange={(e) => guardarDatos(e)} value={personaNueva.nombres} type="text" name="nombres" id="nombres" className='input' />
        <label htmlFor="apellidos">Apellidos</label>
        <input value={personaNueva.apellidos} onChange={(e) => guardarDatos(e)} value={personaNueva.apellidos} type="text" name="apellidos" id="apellidos" className='input' />
        <label htmlFor="documento">Documento</label>
        <input value={personaNueva.documento} onChange={(e) => guardarDatos(e)} value={personaNueva.documento} type="text" name='documento' id='documento' className='input' />
        <button className='btn' onClick={(e) => agregarPersona(e)} >Guardar</button>
      </form>
    </div>
  )
}

export default Agregar;
