import React, { useState, useEffect, createContext } from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "../components/App.js";
import Agregar from "../components/Agregar.js";
import Editar from "../components/Editar.js";
import NoEncontrada from '../components/NoEncontrada.js';

export const PersonasContext = createContext();

export const Routers = () => {
  const [personas,setPersonas]=useState([
    {
      nombres:"Francisco",
      apellidos:"Bello salcedo",
      documento:"1007130073",
    },
    {
      nombres:"robert",
      apellidos:"Morelo",
      documento:"10071303",
    },
  ])
  return (
    <>
      <PersonasContext.Provider value={{personas,setPersonas}}>
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<App />}></Route>
            <Route path='/agregar' element={<Agregar />}></Route>
            <Route path='/editar/:documento' element={<Editar />}></Route>
            <Route path='*' element={<NoEncontrada />}></Route>
          </Routes>
        </BrowserRouter>
      </PersonasContext.Provider>

    </>
  )
}

export default Routers;
